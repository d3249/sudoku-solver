module Sudoku::Plotter
  def self.plot(puzzle)
    result = ''
    (1..9).each do |row|
      (1..9).each do |col|
        result += string_value puzzle.value_at(row, col)
        result += "| " if (col % 3 == 0) && col < 9
      end
      result += "\n"
      result += "--------------------- \n" if (row % 3 == 0) && row < 9
    end
    result
  end

  private

    def self.string_value(value)
      value.zero? ? '  ' : "#{value.to_s} "
    end
end
