module Sudoku
  class Puzzle
    attr_reader :array

    def initialize(array)
      @array = array
    end

    def value_at(row, col)
      @array[pair_to_idx(row, col)]
    end

    def update_value_at(row, col, value)
      @array[pair_to_idx(row, col)] = value
    end

    private

      def pair_to_idx(row, col)
        9 * (row - 1) + col - 1
      end
  end
end
