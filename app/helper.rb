module Sudoku
  module Solver
    module Helper

      SQUARE_INDEXES = {
        1 => [0, 1, 2, 9, 10, 11, 18, 19, 20],
        2 => [3, 4, 5, 12, 13, 14, 21, 22, 23],
        3 => [6, 7, 8, 15, 16, 17, 24, 25, 26],

        4 => [27, 28, 29, 36, 37, 38, 45, 46, 47],
        5 => [30, 31, 32, 39, 40, 41, 48, 49, 50],
        6 => [33, 34, 35, 42, 43, 44, 51, 52, 53],

        7 => [54, 55, 56, 63, 64, 65, 72, 73, 74],
        8 => [57, 58, 59, 66, 67, 68, 75, 76, 77],
        9 => [60, 61, 62, 69, 70, 71, 78, 79, 80],
      }

      # Squares are numbered like this
      #
      #    1 | 2 | 3
      #    ---------
      #    4 | 5 | 6
      #    ---------
      #    7 | 8 | 9
      #
      def square(number, source_array: nil)
        source_array ||= array
        SQUARE_INDEXES[number].map { |idx| source_array[idx] }
      end

      def row(number, source_array: nil)
        source_array ||= array

        (row_indexes number).map { |idx| source_array[idx] }
      end

      def col(number, source_array: nil)
        source_array ||= array

        (col_indexes number).map { |idx| source_array[idx] }
      end

      def square_of(row, col)
        row_idx = row - 1
        col_idx = col - 1
        array_idx = (row_idx * 9) + col_idx

        SQUARE_INDEXES.find { |_, values| values.include? array_idx }&.first
      end

      def options_of_row(row_number)
        reduce(row(row_number)).map(&:options)
      end

      def options_of_col(col_number)
        reduce(col(col_number)).map(&:options)
      end

      def options_of_square(quare_numbrer)
        reduce(square(quare_numbrer)).map(&:options)
      end

      def value_at(row, col)
        idx = (row - 1) * 9 + col - 1
        array[idx]
      end

      def set_single_value_at_set
        raise 'A block is needed' unless block_given?

        (1..9).each do |idx|
          set = yield idx

          single_options = set.flat_map(&:options)
                              .tally
                              .filter { |_, count| count == 1 }
                              .keys

          set.each do |cell|
            single_options.each do |value|
              cell.value = value if cell.options.include? value
            end
          end
        end
      end

      private

        def reduce(set)
          defined_values = set.filter(&:defined?).map(&:value)
          undefined_cells = set.filter { |c| !c.defined? }

          undefined_cells.each do |cell|
            defined_values.each do |assinged_value|
              cell.discard assinged_value
            end
          end

          set
        end

        def col_indexes(number)
          (number - 1).step(80, 9).to_a
        end

        def row_indexes(number)
          start_at = (number - 1) * 9
          end_at = start_at + 9
          (start_at...end_at).to_a
        end

    end
  end
end
