require_relative 'helper'

module Sudoku
  module Solver
    class Cell
      attr_reader :value, :options

      def initialize(value = nil)
        value = nil if value&.zero?

        @value = value if value
        @options = value ? [value] : (1..9).to_a
      end

      def discard(discarted)
        @options.delete discarted

        raise 'All values have been discarded. Method failed' if @options.empty?

        if @options.size == 1
          @value = @options.first
        end

        @options
      end

      def defined?
        !@value.nil?
      end

      def value=(value)
        if @value.nil?
          @value = value
          @options = [value]
        end
      end

      def ==(other)
        @options == other.options
      end
    end

    class Naive
      include Sudoku::Solver::Helper

      def initialize(puzzle)
        @array = puzzle.array.map { |v| Cell.new(v) }
      end

      def array
        @array.map { |c| c.value ? c.value : 0 }
      end

      def cell_array
        @array
      end

      def solved?
        !(cell_array_values.include? nil)
      end

      def solve
        keep_trying = !solved?
        undefined_at_start_of_loop = undefined_cells

        while undefined_at_start_of_loop > 0 && keep_trying

          undefined_at_start_of_loop = undefined_cells

          reduce_squares
          reduce_rows
          reduce_columns

          set_single_value_at_cols
          set_single_value_at_rows
          set_single_value_at_squares

          keep_trying = undefined_at_start_of_loop != undefined_cells
        end
      end

      private

        def cell_array_values
          @array.map(&:value)
        end

        def undefined_cells
          cell_array_values.filter { |c| c.nil? }.count
        end

        def reduce_set
          (1..9).each do |index|
            reduce yield(index)
          end
        end

        def reduce_squares
          reduce_set { |idx| square(idx, source_array: @array) }
        end

        def reduce_rows
          reduce_set { |idx| row(idx, source_array: @array) }
        end

        def reduce_columns
          reduce_set { |idx| col(idx, source_array: @array) }
        end

        def set_single_value_at_rows
          set_single_value_at_set { |row_number| row(row_number, source_array: @array) }
        end

        def set_single_value_at_cols
          set_single_value_at_set { |col_number| col(col_number, source_array: @array) }
        end

        def set_single_value_at_squares
          set_single_value_at_set { |square_number| square(square_number, source_array: @array) }
        end
    end
  end
end
