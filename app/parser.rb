require_relative 'puzzle'

module Sudoku::Parser
  def self.load_csv(path)
    body = []

    File.foreach(path) do |line|
      body += line.split(/\D+/).map &:to_i
    end

    Sudoku::Puzzle.new(body)
  end
end
