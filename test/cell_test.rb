require 'minitest/autorun'
require 'minitest/assertions'

require_relative '../app/solver'

class Sudoku::Solver::CellTest < Minitest::Test
  FULL_SET = [1, 2, 3, 4, 5, 6, 7, 8, 9].freeze

  def setup
    @cell = Sudoku::Solver::Cell.new
  end

  def test_it_has_all_options_at_initialization

    assert_equal FULL_SET, @cell.options
  end

  def test_an_option_can_be_eliminated
    discarded = FULL_SET.shuffle.first

    @cell.discard discarded

    refute @cell.options.include? discarded
  end

  def test_after_all_options_but_one_discarded_the_value_is_assinged
    expected = discard_all_but_one

    assert_equal expected, @cell.value
  end

  def test_exception_is_raised_if_all_values_are_discarded
    exception = assert_raises { FULL_SET.each { |v| @cell.discard v } }

    assert_equal 'All values have been discarded. Method failed', exception.message
  end

  def test_a_cell_can_answer_if_it_is_defined
    refute @cell.defined?

    discard_all_but_one

    assert @cell.defined?
  end

  def test_a_cell_can_be_initialized_with_a_number_and_is_defined
    value = FULL_SET.shuffle.first
    cell = Sudoku::Solver::Cell.new(value)

    assert cell.defined?
    assert_equal value, cell.value
    assert_equal [value], cell.options
  end

  def test_a_cell_initialized_with_zero_is_undefined
    cell = Sudoku::Solver::Cell.new(0)

    refute cell.defined?
    assert_nil cell.value
    assert_equal FULL_SET, cell.options
  end

  def test_it_implements_double_equals_correctly_equal
    cel_1 = Sudoku::Solver::Cell.new 6
    cel_2 = Sudoku::Solver::Cell.new 6

    assert cel_1 == cel_2
  end

  def test_it_implements_double_equals_correctly_different_1
    cel_1 = Sudoku::Solver::Cell.new 6
    cel_2 = Sudoku::Solver::Cell.new 5

    refute cel_1 == cel_2
  end

  def test_it_implements_double_equals_correctly_different_2
    cel_1 = Sudoku::Solver::Cell.new 6
    cel_2 = Sudoku::Solver::Cell.new

    refute cel_1 == cel_2
  end

  def test_the_value_can_be_set_directly
    expected_value = rand(1..9)
    @cell.value = expected_value

    assert_equal expected_value, @cell.value
    assert_equal [expected_value], @cell.options
  end

  def test_once_set_value_can_not_be_changed
    @cell.value = 1
    @cell.value = 9

    assert_equal 1, @cell.value
  end

  private

    def discard_all_but_one
      discarded = FULL_SET.shuffle
      expected = discarded.shift

      discarded.each { |v| @cell.discard v }
      expected
    end
end
