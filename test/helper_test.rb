require 'minitest/autorun'
require 'minitest/assertions'

require_relative '../app/solver'
require_relative '../app/helper'

class Sudoku::Solver::HelperTest < Minitest::Test
  TO_CELL = -> (v) { Sudoku::Solver::Cell.new v }

  def self.make_cells(array)
    array.map(&TO_CELL).freeze
  end

  BODY = [0, 0, 7, 0, 4, 0, 0, 6, 2,
          3, 4, 0, 7, 1, 6, 9, 0, 8,
          0, 0, 0, 0, 0, 8, 0, 0, 7,
          0, 0, 0, 9, 0, 7, 3, 0, 0,
          0, 0, 0, 4, 0, 0, 0, 0, 0,
          0, 0, 8, 0, 3, 0, 0, 0, 0,
          6, 0, 0, 0, 2, 5, 0, 0, 9,
          9, 0, 0, 0, 0, 4, 5, 2, 0,
          4, 2, 0, 3, 0, 0, 0, 1, 0]

  SQUARE_1 = make_cells [0, 0, 7,
                         3, 4, 0,
                         0, 0, 0,]

  SQUARE_2 = make_cells [0, 4, 0,
                         7, 1, 6,
                         0, 0, 8,]

  SQUARE_3 = make_cells [0, 6, 2,
                         9, 0, 8,
                         0, 0, 7,]

  SQUARE_4 = make_cells [0, 0, 0,
                         0, 0, 0,
                         0, 0, 8,]

  SQUARE_5 = make_cells [9, 0, 7,
                         4, 0, 0,
                         0, 3, 0,]

  SQUARE_6 = make_cells [3, 0, 0,
                         0, 0, 0,
                         0, 0, 0,]

  SQUARE_7 = make_cells [6, 0, 0,
                         9, 0, 0,
                         4, 2, 0,]

  SQUARE_8 = make_cells [0, 2, 5,
                         0, 0, 4,
                         3, 0, 0,]

  SQUARE_9 = make_cells [0, 0, 9,
                         5, 2, 0,
                         0, 1, 0,]

  ROW_1 = make_cells (BODY.slice(0, 9)).freeze
  ROW_2 = make_cells (BODY.slice(9, 9)).freeze
  ROW_3 = make_cells (BODY.slice(18, 9)).freeze
  ROW_4 = make_cells (BODY.slice(27, 9)).freeze
  ROW_5 = make_cells (BODY.slice(36, 9)).freeze
  ROW_6 = make_cells (BODY.slice(45, 9)).freeze
  ROW_7 = make_cells (BODY.slice(54, 9)).freeze
  ROW_8 = make_cells (BODY.slice(63, 9)).freeze
  ROW_9 = make_cells (BODY.slice(72, 9)).freeze

  COL_1 = make_cells [0, 3, 0, 0, 0, 0, 6, 9, 4]
  COL_2 = make_cells [0, 4, 0, 0, 0, 0, 0, 0, 2]
  COL_3 = make_cells [7, 0, 0, 0, 0, 8, 0, 0, 0]
  COL_4 = make_cells [0, 7, 0, 9, 4, 0, 0, 0, 3]
  COL_5 = make_cells [4, 1, 0, 0, 0, 3, 2, 0, 0]
  COL_6 = make_cells [0, 6, 8, 7, 0, 0, 5, 4, 0]
  COL_7 = make_cells [0, 9, 0, 3, 0, 0, 0, 5, 0]
  COL_8 = make_cells [6, 0, 0, 0, 0, 0, 0, 2, 1]
  COL_9 = make_cells [2, 8, 7, 0, 0, 0, 9, 0, 0]

  class DummySolver
    include Sudoku::Solver::Helper

    attr_reader :array

    def initialize
      @array = Sudoku::Solver::HelperTest.make_cells BODY
    end
  end

  def setup
    @sut = DummySolver.new
  end

  def test_it_can_recover_all_squares
    (1..9).each do |square_number|
      square = Sudoku::Solver::HelperTest.const_get("SQUARE_#{square_number}")
      assert_equal square, @sut.square(square_number)
    end
  end

  def test_it_can_recover_square_from_given_array
    custom_array = (1..9).flat_map { |row| (1..9).map { row } }
    expected = [1, 1, 1, 2, 2, 2, 3, 3, 3]

    assert_equal expected, @sut.square(1, source_array: custom_array)
  end

  def test_it_can_recover_row_from_given_array
    custom_array = (1..9).flat_map { |row| (1..9).map { row } }
    expected = [4, 4, 4, 4, 4, 4, 4, 4, 4]

    assert_equal expected, @sut.row(4, source_array: custom_array)
  end

  def test_it_can_recover_col_from_given_array
    custom_array = (1..9).flat_map { |row| (1..9).map { row } }
    expected = [1, 2, 3, 4, 5, 6, 7, 8, 9]

    assert_equal expected, @sut.col(9, source_array: custom_array)
  end

  def test_it_can_recover_all_rows
    (1..9).each do |row_number|
      row = Sudoku::Solver::HelperTest.const_get("ROW_#{row_number}")
      assert_equal row, @sut.row(row_number)
    end
  end

  def test_it_can_recover_all_columns
    (1..9).each do |col_number|
      col = Sudoku::Solver::HelperTest.const_get("COL_#{col_number}")
      assert_equal col, @sut.col(col_number)
    end
  end

  def test_it_recovers_the_square_of_a_coordinate
    # [row, col, square]
    values = [[3, 3, 1], [3, 6, 2], [3, 9, 3], [6, 3, 4], [6, 6, 5], [6, 9, 6], [9, 3, 7], [9, 6, 8], [9, 9, 9]]

    values.each do |values|
      assert_equal values[2], @sut.square_of(values[0], values[1])
    end
  end

  def test_it_can_reduce_a_line
    unassinged = [1, 3, 5, 8, 9]
    expected_options = [unassinged, unassinged, [7], unassinged, [4], unassinged, unassinged, [6], [2]]

    result = @sut.options_of_row 1

    assert_equal expected_options, result
  end

  def test_it_can_reduce_a_col
    unassinged = [1, 2, 5, 7, 8]
    expected_options = [unassinged, [3], unassinged, unassinged, unassinged, unassinged, [6], [9], [4]]

    result = @sut.options_of_col 1

    assert_equal expected_options, result
  end

  def test_it_can_reduce_a_square
    unassigned = [1, 2, 5, 6, 8, 9]
    expected_options = [unassigned, unassigned, [7], [3], [4], unassigned, unassigned, unassigned, unassigned]

    result = @sut.options_of_square 1

    assert_equal expected_options, result
  end

  def test_it_can_find_options_on_single_cell_on_a_set
    row_number = 1
    discarded_value = 8
    row = @sut.row(row_number)
    (1..8).each { |idx| row[idx].discard discarded_value }

    @sut.set_single_value_at_set { row }

    assert_equal discarded_value, @sut.value_at(1, 1).value
  end

end
