require 'minitest/autorun'
require 'minitest/assertions'

require_relative '../app/puzzle'
require_relative '../app/parser'
require_relative '../app/solver'

class Sudoku::Solver::NaiveTest < Minitest::Test

  def test_it_can_recover_inner_array
    puzzle = load_puzzle 'solved'

    naive_solver = Sudoku::Solver::Naive.new puzzle

    assert_equal puzzle.array, naive_solver.array
  end

  def test_a_puzle_is_not_solved_if_it_has_une_undefined_cell
    naive_solver = load_naive_solver 'single_cell'

    refute naive_solver.solved?
  end

  def test_a_puzzle_is_solved_if_all_cells_are_solved
    naive_solver = load_naive_solver 'solved'

    assert naive_solver.solved?
  end

  def test_it_can_solve_single_cell_missing
    naive_solver = load_naive_solver 'single_cell'

    naive_solver.solve

    assert naive_solver.solved?
  end

  def test_it_can_solve_single_line_missing
    naive_solver = load_naive_solver 'single_line'

    naive_solver.solve

    assert naive_solver.solved?
  end

  private

    def load_naive_solver(file)
      Sudoku::Solver::Naive.new load_puzzle file
    end

    def load_puzzle(file)
      Sudoku::Parser.load_csv "test/test_files/#{file}.csv"
    end
end
