require 'minitest/autorun'
require_relative '../app/puzzle'
require_relative '../app/solver'
require_relative '../app/plotter'

class Sudoku::PlotterTest < Minitest::Test
  BODY = [0, 0, 7, 0, 4, 0, 0, 6, 2,
          3, 4, 0, 7, 1, 6, 9, 0, 8,
          0, 0, 0, 0, 0, 8, 0, 0, 7,
          0, 0, 0, 9, 0, 7, 3, 0, 0,
          0, 0, 0, 4, 0, 0, 0, 0, 0,
          0, 0, 8, 0, 3, 0, 0, 0, 0,
          6, 0, 0, 0, 2, 5, 0, 0, 9,
          9, 0, 0, 0, 0, 4, 5, 2, 0,
          4, 2, 0, 3, 0, 0, 0, 1, 0,]

  PUZZLE = Sudoku::Puzzle.new BODY

  EXPECTED = "    7 |   4   |   6 2 \n" \
             "3 4   | 7 1 6 | 9   8 \n" \
             "      |     8 |     7 \n" \
             "--------------------- \n" \
             "      | 9   7 | 3     \n" \
             "      | 4     |       \n" \
             "    8 |   3   |       \n" \
             "--------------------- \n" \
             "6     |   2 5 |     9 \n" \
             "9     |     4 | 5 2   \n" \
             "4 2   | 3     |   1   \n"

  def test_it_plots_correctly
    result = Sudoku::Plotter.plot PUZZLE

    assert_equal EXPECTED, result
  end

  def test_it_plots_a_solver
    solver = Sudoku::Solver::Naive.new PUZZLE

    result = Sudoku::Plotter.plot solver

    assert_equal EXPECTED, result
  end
end
