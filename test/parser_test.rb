require 'minitest/autorun'

require_relative '../app/sudoku'

class Sudoku::ParserTest < Minitest::Test
  PATH = 'test/test_files/test_1.csv'

  def setup
    @puzzle = Sudoku::Parser.load_csv PATH
  end

  def test_it_loads
    expected = [0, 0, 7, 0, 4, 0, 0, 6, 2,
                3, 4, 0, 7, 1, 6, 9, 0, 8,
                0, 0, 0, 0, 0, 8, 0, 0, 7,
                0, 0, 0, 9, 0, 7, 3, 0, 0,
                0, 0, 0, 4, 0, 0, 0, 0, 0,
                0, 0, 8, 0, 3, 0, 0, 0, 0,
                6, 0, 0, 0, 2, 5, 0, 0, 9,
                9, 0, 0, 0, 0, 4, 5, 2, 0,
                4, 2, 0, 3, 0, 0, 0, 1, 0,]

    assert_equal expected, @puzzle.array
  end

end
