require 'minitest/autorun'
require_relative '../app/puzzle'

class Sudoku::PuzzleTest < Minitest::Test

  BODY = [0, 0, 7, 0, 4, 0, 0, 6, 2,
          3, 4, 0, 7, 1, 6, 9, 0, 8,
          0, 0, 0, 0, 0, 8, 0, 0, 7,
          0, 0, 0, 9, 0, 7, 3, 0, 0,
          0, 0, 0, 4, 0, 0, 0, 0, 0,
          0, 0, 8, 0, 3, 0, 0, 0, 0,
          6, 0, 0, 0, 2, 5, 0, 0, 9,
          9, 0, 0, 0, 0, 4, 5, 2, 0,
          4, 2, 0, 3, 0, 0, 0, 1, 0,]

  def setup
    @puzzle = Sudoku::Puzzle.new BODY
  end

  def test_it_recovers_value
    assert_equal 9, @puzzle.value_at(4, 4)
    assert_equal 4, @puzzle.value_at(9, 1)
  end

  def test_it_updates_value
    assert_equal 0, @puzzle.value_at(1, 1)
    @puzzle.update_value_at(1, 1, 9)

    assert_equal 9, @puzzle.value_at(1, 1)
  end
end
