# Sudoku Solver

This was an exercise to practice Ruby.

The strategy programmed is based on pure elimination, not doing any backtrace (needed for more complex puzzles).

The project is composed by a parser, a plotter and some models (`Puzzle`, `Cell`, `Solver::Naive`).

## Licence

This project is released under (GPL3)[https://www.gnu.org/licenses/gpl.txt]

